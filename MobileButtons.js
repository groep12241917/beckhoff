import { directions } from "./directions.js"
//test
export class MobileButtons {
    buttonsElement;
    snake;
    buttons;

    constructor(buttonsElement, snake) {
        this.buttonsElement = buttonsElement;
        this.snake = snake;
        const upButton = buttonsElement.querySelector(".up");
        const rightButton = buttonsElement.querySelector(".right");
        const downButton = buttonsElement.querySelector(".down");
        const leftButton = buttonsElement.querySelector(".left");

        this.buttonsElement.classList.remove("hidden")

        this.buttons = [upButton, rightButton, downButton, leftButton]
        this.buttons.forEach((button) => {
            button.addEventListener("click", (event) => {
                switch (event.target.classList[1]) {
                    case "up":
                        snake.turn(directions.UP);
                        break;
                    case "right":
                        snake.turn(directions.RIGHT);
                        break;
                    case "down":
                        snake.turn(directions.DOWN);
                        break;
                    case "left":
                        snake.turn(directions.LEFT)
                        break;
                }
            })
        })
    }

}
