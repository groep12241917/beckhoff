import { Obstacle } from "./Obstacle";
export class BadFood extends Obstacle {
    constructor(canvas, Snake, obstacles){
        super(canvas, Snake, obstacles);
        this.element.classList.add("badFood")
    }

    onColission(){
        this.snake.schrink = true
        this.remove();
    }
}