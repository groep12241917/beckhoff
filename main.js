import { Snake } from "./Snake.js";
import { Food } from "./Food.js";
import { directions } from "./directions.js";
import { MobileButtons } from "./MobileButtons.js";
import { Muur } from "./Muur.js";
import { Kast } from "./Kast.js";
import { BadFood } from "./BadFood.js";

const background = function (canvas) {
    for (let i = 1; i <= 21; i++) {
        for (let j = 1; j <= 11; j++) {
            const div = document.createElement("div");
            div.style.gridArea = `${j}/${i}`;
            div.classList.add("item");
            canvas.appendChild(div);
        }
    }
};

document.addEventListener("keydown", function (event) {
    const key = event.key;

    if (key === "ArrowUp" || key === "z") snake.turn(directions.UP);
    if (key === "ArrowRight" || key === "d") snake.turn(directions.RIGHT);
    if (key === "ArrowDown" || key === "s") snake.turn(directions.DOWN);
    if (key === "ArrowLeft" || key === "q") snake.turn(directions.LEFT);
});


//scoreTeller
let lastScore = 0
let framesAlife;
const scoreFood = 100;
const scoreFactor = 0.02;
const scoreDisplay = document.querySelector(".score-display");
let totaalWeetjes = 0;

const weetjes = document.querySelectorAll(".weet")

let food;
let snake;
let obstacles;
let buttons;
const canvas = document.querySelector(".struiken section");
const groups = ["#00ff99", "#ff00ff", "#ffff00", "#0066ff"];
let group = 0;


const gameOver = function () {

    buttons.buttonsElement.classList.add("hidden")

    const gameOver = document.createElement("div");
    gameOver.classList.add("gameOver");

    const gameOverTekst = document.createElement("p");
    gameOverTekst.innerText = "GAMEOVER";

    const opnieuw = document.createElement("button");
    opnieuw.style.backgroundColor = "#93BAAA";
    opnieuw.innerText = "Speel opnieuw";
    opnieuw.id = "opnieuw";
    opnieuw.disabled = false;
    opnieuw.addEventListener("click", () => {
        location.href = '/'
    });

    const prijs = document.createElement("button");
    prijs.innerText = "Ontvang prijs";
    prijs.style.backgroundColor = "#F6ADB2";
    prijs.id = "prijs";
    prijs.addEventListener("click", gameStart);

    const weetjesContainer = document.createElement("div");
    weetjesContainer.classList.add("weetjes-container");
    weetjes.forEach(weetje => {
        const weetjeElement = document.createElement("p");
        weetjeElement.innerText = weetje.innerText;
        weetjeElement.classList.add("weet");
        weetjesContainer.appendChild(weetjeElement);
    });

    gameOver.appendChild(gameOverTekst);
    gameOver.appendChild(weetjesContainer);
    gameOver.appendChild(opnieuw);
    gameOver.appendChild(prijs);

    canvas.appendChild(gameOver);

    let food = canvas.querySelector(".food");

    if (food) {
        canvas.removeChild(food);
    }
}

const checkObstacleCollision = function (snake, obstacles) {
    for (let obstacle of obstacles) {
        if (snake.x === obstacle.x && snake.y === obstacle.y) {
            obstacle.onColission();
            break
        }
    }

};

const updateSnakeSpeed = function (snake) {
    const baseSpeed = 200;
    const speedIncrement = 1.2;
    snake.speed = Math.max(
        baseSpeed - Math.floor((snake.score / 100) * speedIncrement),
        50
    );
};

const spawnMuren = function (obstacles, canvas, snake) {
    const thresholds = [300, 400, 750, 1500]; //hier aanpassen voor meer obstakels
    for (let threshold of thresholds) {
        if (
            snake.score >= threshold &&
            !obstacles.some((ob) => ob.spawnedAt === threshold)
        ) {
            const obstacle = new Muur(canvas, snake, obstacles);
            obstacle.spawnedAt = threshold;
            obstacles.push(obstacle);
        }
    }
};


const spawnBadFood = function (obstacles, canvas, snake) {
    const thresholds = [50, 150, 250, 550, 1550]; //hier aanpassen voor meer obstakels
    for (let threshold of thresholds) {
        if (
            snake.score >= threshold &&
            !obstacles.some((ob) => ob.spawnedAt === threshold)
        ) {
            const obstacle = new BadFood(canvas, snake, obstacles);
            obstacle.spawnedAt = threshold;
            obstacles.push(obstacle);
        }
    }
};

const spawnKasten = function (obstacles, canvas, snake) {
    const thresholds = [100, 200, 1000, 2000]; //hier aanpassen voor meer obstakels


    for (let threshold of thresholds) {
        if (
            snake.score >= threshold &&
            !obstacles.some((ob) => ob.spawnedAt === threshold)
        ) {

            for (let i = 0; i < 2; i++) {
                const obstacle = new Kast(canvas, snake, obstacles, groups[group]);
                obstacle.spawnedAt = threshold;
                obstacles.push(obstacle);
            }
            group++

        }
    }
};

const toonweetje = function () {
    if (totaalWeetjes < weetjes.length) {
        weetjes[totaalWeetjes].classList.remove("hidden");
        weetjes[totaalWeetjes].classList.add("weetjes");
        totaalWeetjes += 1;
    }
}

const gameLoop = function () {
    snake.move();

    framesAlife++;
    snake.score = scoreFactor * framesAlife + snake.score;
    scoreDisplay.innerText = Math.round(snake.score);

    updateSnakeSpeed(snake);
    spawnMuren(obstacles, canvas, snake);
    spawnKasten(obstacles, canvas, snake);
    spawnBadFood(obstacles, canvas, snake);

    checkObstacleCollision(snake, obstacles)

    if (snake.score - lastScore >= 100) {
        toonweetje();
        lastScore = snake.score
        obstacles.push(new Food(canvas, snake, obstacles))
    }

    if (!snake.gameOver) {
        setTimeout(gameLoop, snake.speed);
    } else {
        gameOver();
    }
}

const removeSnake = function () {
    if (snake && snake.body) {
        snake.body.forEach((e) => {
            if (e.body.parentNode === canvas) {
                canvas.removeChild(e.body);
            }
        });
    }
}

const gameStart = function () {
    canvas.innerHTML = ""; // Clear the canvas before starting the game
    removeSnake();
    background(canvas);

    snake = new Snake(5, 7, 6, canvas);

    obstacles = [];
    framesAlife = 0;
    group = 0;


    buttons = new MobileButtons(
        document.querySelector(".mobileButtons"),
        snake
    );


    food = new Food(canvas, snake, obstacles); // Pass obstacles to Food
    obstacles.push(food)


    const weetjes = document.querySelectorAll(".weet");
    weetjes.forEach((e) => e.classList.add("hidden"));
    gameLoop();
};

gameStart();
