import {Obstacle} from "./Obstacle.js";

export class Kast extends Obstacle{
    constructor(canvas, Snake, obstacles, group) {
        super(canvas, Snake, obstacles);
        this.element.classList.add("kast")
        this.group = group
        this.element.style.backgroundColor = group
    }

    onColission(){

            let newx = 0
            let newy = 0
            this.obstacles.forEach((e) => {
                if (e.group === this.group && !(e.x === this.x && e.y === this.y)){
                    this.snake.x = e.x
                    this.snake.y = e.y
                }
            })
        }


}