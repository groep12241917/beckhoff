export class SnakeDeel {
    constructor(x, y, canvas) {
        this.x = x;
        this.y = y;

        this.body = document.createElement("div");
        this.body.classList.add("chef")
        canvas.appendChild(this.body);
        this.update();
    }

    update() {
        this.body.style.gridArea = `${this.y}/${this.x}`;
    }

    xY(x, y) {
        this.x = x;
        this.y = y;
        this.update();
    }
}