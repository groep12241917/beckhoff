import {Obstacle} from "./Obstacle.js";

export class Food extends Obstacle{
    constructor(canvas, snake, obstacles) {
        super(canvas, snake, obstacles)

        this.images = [
            "url(/images/pot.png)",
            "url(/images/pot2.png)",
            "url(/images/pot3.png)",
            "url(/images/pot4.png)",
            "url(/images/pot5.png)",
            "url(/images/pot6.png)",
            "url(/images/pot7.png)",
            "url(/images/pot8.png)",
            "url(/images/pot9.png)"
        ];
        this.setRandomImage();

        this.element.classList.add("food")

    }

    setRandomImage() {
        const randomIndex = Math.floor(Math.random() * this.images.length);
        this.element.style.backgroundImage = this.images[randomIndex];
    }

    onColission(){
        this.snake.collided = true;
        this.remove();
        this.snake.score += 100

    }


}
