export class Obstacle {
    constructor(canvas, Snake, obstacles) {
        this.canvas = canvas;
        this.snake = Snake;
        this.obstacles = obstacles;

        this.element = document.createElement("div");
        this.element.className = "obstacle";

        this.randomizePosition();
        this.render();
    }

    randomizePosition() {
        const maxX = 20; // Max X-coordinate
        const maxY = 10; // Max Y-coordinate
        let overlap = true;
        while (overlap) {
            overlap = false;
            this.x = Math.floor(Math.random() * maxX) + 1;
            this.y = Math.floor(Math.random() * maxY) + 1;

            this.snake.body.forEach(part => {
                if (part.x === this.x && part.y === this.y) {
                    overlap = true;
                }
            });//geen obstacle op een bodypart
            
            if (
                this.x < this.snake.body[0].x + 2 &&
                this.x > this.snake.body[0].x - 2 &&
                this.y < this.snake.body[0].y + 2 &&
                this.y > this.snake.body[0].y - 2
            ) {
                overlap = true;
            }//zone rond hoofd waar geen obstacle kan spawnen
            


            this.obstacles.forEach((obstacle) => {
                if (obstacle.x === this.x && obstacle.y === this.y) {
                    overlap = true;
                }
            });

            // Check overlap with food
            if (this.food && this.food.x === this.x && this.food.y === this.y) {
                overlap = true;
            }
        }
    }

    render() {
        this.element.style.gridArea = `${this.y}/${this.x}`;
        this.canvas.appendChild(this.element);
    }

    remove() {
        this.element.remove();
    }
}
