import { Obstacle } from "./Obstacle";
export class Muur extends Obstacle {
    constructor(canvas, Snake, obstacles){
        super(canvas, Snake, obstacles);
        this.element.classList.add("muur")
    }

    onColission(){
        this.snake.die()
    }
}