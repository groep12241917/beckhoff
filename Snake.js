import { SnakeDeel } from "./SnakeDeel.js";
import { directions } from "./directions.js";

export class Snake {
    body = [];
    gameOver = false;
    speed = 200; // startsnelheid
    collided = false;
    moved = true;
    schrink = false;
    score = 0

    constructor(length, startX, startY, canvas) {
        this.x = startX;
        this.y = startY;
        this.canvas = canvas;
        this.direction = directions.RIGHT;
        let gameoverscreen = canvas.querySelector(".gameOver");

        if (gameoverscreen) {
            canvas.removeChild(gameoverscreen);
        }

        for (let i = 0; i < length; i++) {
            this.body.push(new SnakeDeel(this.x - i, this.y, canvas));
        }
    }

    move() {
        if (!this.gameOver) {
            switch (this.direction) {
                case directions.UP:
                    this.y--;
                    break;
                case directions.RIGHT:
                    this.x++;
                    break;
                case directions.DOWN:
                    this.y++;
                    break;
                case directions.LEFT:
                    this.x--;
                    break;
            }
            if (1 > this.y || this.y > 11 || 1 > this.x || this.x > 21) {
                this.die();
            }
            if (!this.collided) {
                this.body[this.body.length - 1].xY(this.x, this.y); // change location last bodypart
                this.body.unshift(this.body.pop()); // change location in list
            } else {
                let bodypart = new SnakeDeel(this.x, this.y, this.canvas);
                this.body.unshift(bodypart);
                this.collided = false;
            }

            if (this.schrink){
                this.canvas.removeChild(this.body.pop().body)
                this.schrink = false
            }
            // check if collided with self
            this.body.forEach((bodyDeel) => {
                if (
                    this.body[0] != bodyDeel &&
                    this.x === bodyDeel.x &&
                    this.y === bodyDeel.y
                ) {
                    this.die();
                }
            });
        }
        this.moved = true;
    }

    turn(direction) {
        if (!this.moved) return;
        if (
            !(
                direction - this.direction === 2 || // kan niet in zichzelf draaien
                direction - this.direction === -2
            )
        ) {
            this.direction = direction;
        }
        this.moved = false; // zorg ervoor dat je niet 2 keer van richting kunt veranderen in 1 frame
    }

    die() {
        this.gameOver = true;
    }


}
